import path from "path" //eslint-disable-line node/no-unpublished-import

export default function basename(html) {
    return String(html)
        .replace(/(src|href)="([^ ]+)"/g, (match, tagName, filePath) => {return tagName + "=\"" + path.basename(filePath) + "\""})
}