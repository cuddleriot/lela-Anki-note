import test from "ava"
import {setUp, tearDown, cA,
    cardEnterrar, cardMientrasTanto, cardSixThree, cardCinco, cardLazo, cardSummit,
    INDICATION_1_PIECES, INDICATION_1_PIECES_KNOWN, INDICATION_1_PIECES_LEARNED,
    INDICATION_2_PIECES, INDICATION_2_PIECES_KNOWN, INDICATION_2_PIECES_LEARNED,
    INDICATION_3_PIECES, INDICATION_3_PIECES_KNOWN, INDICATION_3_PIECES_LEARNED,
    INDICATION_4_PIECES, INDICATION_4_PIECES_KNOWN, INDICATION_4_PIECES_LEARNED,
    INDICATION_5_PIECES, INDICATION_5_PIECES_KNOWN,
    INDICATION_5_PLUS_PIECES_KNOWN, INDICATION_5_PLUS_PIECES_LEARNED} from "./_testUtils.js"


test.before(t => {
    setUp()
})

test.after.always(t => {
    tearDown()
})

test("test with one word and empty tags", t => {
    //given
    const card = cardEnterrar("", "")

    //then
    t.deepEqual(cA([]), cA(card.originalTags))
    t.deepEqual(cA(card.originalTags), cA(card.tags), "no tags should have been added or removed")
})

test("test with one word and language tag", t => {
    //given
    const card = cardEnterrar("", "language_EN-ES")

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags), "no tags should have been added or removed")
})

test("test with one word and a correct one-piece tag, expecting no change", t => {
    //given
    const card = cardEnterrar(INDICATION_1_PIECES)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags), "no tags should have been added or removed")
})

test("test with one word and a 'wrong' one-piece tag, expecting no change - 'the user knows what they're doing'", t => {
    //given
    const card = cardEnterrar("", INDICATION_2_PIECES)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags), "no tags should have been added or removed")
})

test("test with one word and another 'wrong' one-piece tag, expecting no change - 'the user knows what they're doing'", t => {
    //given
    const card = cardEnterrar(INDICATION_3_PIECES)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags), "no tags should have been added or removed")
})

test("test with two pieces and no implicit/explicit piece tags, expecting 2 pieces tags - 'auto-detect'", t => {
    //given
    const card = cardMientrasTanto()

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags.concat([INDICATION_2_PIECES_LEARNED, INDICATION_2_PIECES_KNOWN])), "there should be two new tags to indicate the two pieces")
})

test("test with two pieces and an explicit 1 piece tag for one side, expecting 2 pieces tag for the other side - 'auto-detect'", t => {
    //given
    const card = cardMientrasTanto(INDICATION_1_PIECES_LEARNED)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags.concat([INDICATION_2_PIECES_KNOWN])), "2 pieces tag should have been added for the known side")
})

test("test with two pieces and explicit 1-piece tags for both sides, expecting no change - 'the user knows what they're doing'", t => {
    //given
    const card = cardMientrasTanto(INDICATION_1_PIECES_LEARNED + " " + INDICATION_1_PIECES_KNOWN)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags), "no tags should have been added or removed")
})

test("test with two pieces and an explicit 1 piece tag, expecting no change - 'the user knows what they're doing'", t => {
    //given
    const card = cardMientrasTanto(INDICATION_1_PIECES)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags), "no tags should have been added or removed")
})

test("test with two pieces and an explicit 2 piece tag, expecting no change - 'superfluous but correct'", t => {
    //given
    const card = cardMientrasTanto(INDICATION_2_PIECES)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags), "no tags should have been added or removed")
})

test("test with two pieces and explicit 2-pieces tags, expecting no change - 'superfluous but correct'", t => {
    //given
    const card = cardMientrasTanto(INDICATION_2_PIECES_KNOWN + " " + INDICATION_2_PIECES_LEARNED)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags), "no tags should have been added or removed")
})

test("test with two pieces and explicit 3-pieces tags, expecting no change - 'the user knows what they're doing'", t => {
    //given
    const card = cardMientrasTanto(INDICATION_3_PIECES_KNOWN + " " + INDICATION_3_PIECES_LEARNED)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags), "no tags should have been added or removed")
})

test("test with two pieces and an explicit 3-pieces tag, expecting no change - 'the user knows what they're doing'", t => {
    //given
    const card = cardMientrasTanto(INDICATION_3_PIECES)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags), "no tags should have been added or removed")
})

test("test with two pieces and an explicit 4-pieces tag, expecting no change - 'the user knows what they're doing'", t => {
    //given
    const card = cardMientrasTanto(INDICATION_4_PIECES)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags), "no tags should have been added or removed")
})

test("test with two pieces and explicit 4-pieces tags, expecting no change - 'the user knows what they're doing'", t => {
    //given
    const card = cardMientrasTanto(INDICATION_4_PIECES_LEARNED + " " + INDICATION_4_PIECES_KNOWN)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags), "no tags should have been added or removed")
})

test("test with two pieces and explicit 5-pieces tags, expecting no change on that side - 'the user knows what they're doing'", t => {
    //given
    const card = cardMientrasTanto(INDICATION_5_PIECES_KNOWN)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags.concat([INDICATION_2_PIECES_LEARNED])))
})

test("test with two pieces and explicit 5-plus-pieces tags, expecting no change on that side - 'the user knows what they're doing'", t => {
    //given
    const card = cardMientrasTanto(INDICATION_5_PLUS_PIECES_LEARNED)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags.concat([INDICATION_2_PIECES_KNOWN])))
})

test("test with three pieces and no piece tags, expecting 3-piece tags - 'auto-detect'", t => {
    //given
    const card = cardSummit()

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags.concat([INDICATION_3_PIECES_LEARNED, INDICATION_3_PIECES_KNOWN])), "the 3-piece tags should have been added")
})

test("test with three pieces and an explicit 1-piece tag, expecting no change - 'the user knows what they're doing'", t => {
    //given
    const card = cardSummit(INDICATION_1_PIECES)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags))
})

test("test with three pieces and an explicit 2-piece tag, expecting no change on that side - 'the user knows what they're doing'", t => {
    //given
    const card = cardSummit(INDICATION_2_PIECES_LEARNED)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags.concat([INDICATION_3_PIECES_KNOWN])))
})

test("test with three pieces and an explicit 3-piece tag, expecting no change on that side - 'superfluous, but correct'", t => {
    //given
    const card = cardSummit(INDICATION_3_PIECES_KNOWN)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags.concat([INDICATION_3_PIECES_LEARNED])))
})

test("test with three pieces and an explicit 4-piece tag, expecting no change on that side - 'the user knows what they're doing'", t => {
    //given
    const card = cardSummit(INDICATION_4_PIECES)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags))
})

test("test with three pieces and an explicit 5-piece tag, expecting no change on that side - 'the user knows what they're doing'", t => {
    //given
    const card = cardSummit(INDICATION_5_PIECES)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags))
})

test("test with three pieces and an explicit 5-plus-piece tag, expecting no change on that side - 'the user knows what they're doing'", t => {
    //given
    const card = cardSummit(INDICATION_5_PLUS_PIECES_KNOWN)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags.concat([INDICATION_3_PIECES_LEARNED])))
})

test("test with four pieces and no explicit tags, expecting a 4-piece tag on that side - 'auto-detect'", t => {
    //given
    const card = cardLazo()

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags.concat([INDICATION_4_PIECES_KNOWN])))
})

test("test with four pieces and an explicit 1-piece tag, no change on that side - 'the user knows what they're doing'", t => {
    //given
    const card = cardLazo(INDICATION_1_PIECES_KNOWN)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags))
})

test("test with four pieces and an explicit 2-piece tag, no change on that side - 'the user knows what they're doing'", t => {
    //given
    const card = cardLazo(INDICATION_2_PIECES)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags))
})

test("test with four pieces and an explicit 3-piece tag, no change on that side - 'the user knows what they're doing'", t => {
    //given
    const card = cardLazo(INDICATION_3_PIECES_KNOWN)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags))
})

test("test with four pieces and an explicit 4-piece tag, no change on that side - 'superfluous, but correct'", t => {
    //given
    const card = cardLazo(INDICATION_4_PIECES_KNOWN)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags))
})

test("test with four pieces and an explicit 5-piece tag, no change on that side - 'the user knows what they're doing'", t => {
    //given
    const card = cardLazo(INDICATION_5_PIECES_KNOWN)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags))
})

test("test with four pieces and an explicit 5-plus-pieces tag, no change on that side - 'the user knows what they're doing'", t => {
    //given
    const card = cardLazo(INDICATION_5_PLUS_PIECES_KNOWN)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags))
})

test("test with five pieces and no explicit tags, expecting a 5-piece tag - 'auto-detect'", t => {
    //given
    const card = cardCinco()

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags.concat([INDICATION_5_PIECES_KNOWN])))
})

test("test with five pieces and an explicit 1-piece tag, no change on that side - 'the user knows what they're doing'", t => {
    //given
    const card = cardCinco(INDICATION_1_PIECES_KNOWN)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags))
})

test("test with five pieces and an explicit 2-piece tag, no change on that side - 'the user knows what they're doing'", t => {
    //given
    const card = cardCinco(INDICATION_2_PIECES)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags))
})

test("test with five pieces and an explicit 3-piece tag, no change on that side - 'the user knows what they're doing'", t => {
    //given
    const card = cardCinco(INDICATION_3_PIECES_KNOWN)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags))
})

test("test with five pieces and an explicit 4-piece tag, no change on that side - 'the user knows what they're doing'", t => {
    //given
    const card = cardCinco(INDICATION_4_PIECES)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags))
})

test("test with five pieces and an explicit 5-piece tag, no change on that side - 'superfluous but correct'", t => {
    //given
    const card = cardCinco(INDICATION_5_PIECES_KNOWN)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags))
})

test("test with five pieces and an explicit 5-plus-piece tag, no change on that side - 'the user knows what they're doing'", t => {
    //given
    const card = cardCinco(INDICATION_5_PLUS_PIECES_KNOWN)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags))
})

test("test with six pieces and no explicit tags, expecting a 5-plus-piece tag - 'auto-detect'", t => {
    //given
    const card = cardSixThree()

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags.concat([INDICATION_5_PLUS_PIECES_KNOWN])))
})

test("test with six pieces and an explicit 1-piece tag, expecting no change on that side - 'the user knows what they're doing'", t => {
    //given
    const card = cardSixThree(INDICATION_1_PIECES_KNOWN)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags))
})

test("test with six pieces and an explicit 3-piece tag, expecting no change on that side - 'the user knows what they're doing'", t => {
    //given
    const card = cardSixThree(INDICATION_3_PIECES_KNOWN)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags))
})

test("test with six pieces and an explicit 4-piece tag, expecting no change on that side - 'the user knows what they're doing'", t => {
    //given
    const card = cardSixThree(INDICATION_4_PIECES_KNOWN)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags))
})

test("test with six pieces and an explicit 5-piece tag, expecting no change on that side - 'the user knows what they're doing'", t => {
    //given
    const card = cardSixThree(INDICATION_5_PIECES_KNOWN)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags))
})

test("test with six pieces and an explicit 5-plus-piece tag, expecting no change on that side - 'superfluous but correct'", t => {
    //given
    const card = cardSixThree(INDICATION_5_PLUS_PIECES_KNOWN)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags))
})